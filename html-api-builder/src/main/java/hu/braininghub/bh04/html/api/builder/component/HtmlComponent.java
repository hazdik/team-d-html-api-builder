/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.html.api.builder.component;

/**
 *
 * @author Attila
 */
public interface HtmlComponent {
    
    String getHTMLCodeAsString();
}
