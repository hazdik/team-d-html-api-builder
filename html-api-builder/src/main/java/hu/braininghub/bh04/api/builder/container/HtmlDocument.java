/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.api.builder.container;

import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlContainer;

/**
 *
 * @author Attila
 */
public class HtmlDocument extends AbstractHtmlContainer {

    public HtmlDocument() {
        super(null);
    }

    @Override
    public String getHTMLCodeAsString() {
        
        htmlCodeBuilder.append("<!DOCTYPE html>")
                .append("<html>")
                .append("<body>")
                .append(super.getHTMLCodeAsString())
                .append("</body>")
                .append("</html>");
        return htmlCodeBuilder.toString();
    }
}
