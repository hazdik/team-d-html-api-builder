/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.bh04.api.builder.container;

import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlComponent;
import hu.braininghub.bh04.html.api.builder.component.AbstractHtmlContainer;

/**
 *
 * @author Attila
 */
public class Table extends AbstractHtmlContainer {

    private final AbstractHtmlComponent[][] components;
    private final String[] headers;

    public Table(String id, int rowNumber, int colNumber, String[] headers, AbstractHtmlContainer parent) {
        super(id, parent);
        components = new AbstractHtmlComponent[rowNumber][colNumber];
        assert headers.length == colNumber;
        this.headers = headers;
    }

    public void removeElementFromIndex(int rowIndex, int colIndex) {
        validateIndexes(rowIndex, colIndex);
        components[rowIndex][colIndex] = null;
    }

    public void addElementToIndex(int rowIndex, int colIndex, AbstractHtmlComponent comp) {

        validateIndexes(rowIndex, colIndex);
        components[rowIndex][colIndex] = comp;
    }

    private void validateIndexes(int rowIndex, int colIndex) {
        if (rowIndex >= components.length) {
            throw new IllegalArgumentException("Non existing row index!");
        }
        if (colIndex >= components[rowIndex].length) {
            throw new IllegalArgumentException("Non existing col index!");
        }
    }

    @Override
    public void addChild(AbstractHtmlComponent component) {
        throw new UnsupportedOperationException("This function not supported in case of this component! Use instead addElementToIndex");
    }

    @Override
    public void remove(AbstractHtmlComponent component) {
        throw new UnsupportedOperationException("This function not supported in case of this component! Use instead removeElementFromIndex");
    }

    @Override
    public String getHTMLCodeAsString() {

        super.getHTMLCodeAsString();

        htmlCodeBuilder.append("<table>");

        for (String h : headers) {
            htmlCodeBuilder.append("<th>")
                    .append(h)
                    .append("</th>");
        }

        for (AbstractHtmlComponent[] row : components) {

            htmlCodeBuilder.append("<tr>");
            for (AbstractHtmlComponent column : row) {
                htmlCodeBuilder.append("<td>")
                        .append(column.getHTMLCodeAsString() != null ? column.getHTMLCodeAsString() : "")
                        .append("</td>");
            }
            htmlCodeBuilder.append("</tr>");
        }

        htmlCodeBuilder.append("</table>");
        return htmlCodeBuilder.toString();
    }

}
